# -*- coding: utf-8 -*-
"""
Created on Thu Jan 28 19:17:59 2016

@author: Filip
"""

import numpy as np
from pybrain.datasets import SupervisedDataSet
import numpy, math
from pybrain.structure import SigmoidLayer, LinearLayer
from pybrain.tools.shortcuts import buildNetwork
from pybrain.supervised.trainers import BackpropTrainer
import pylab

def add_noise(y):
    np.random.seed(14)
    varNoise = np.max(y) - np.min(y)
    y_noisy = y + 0.1*varNoise*np.random.normal(0,1,len(y))
    return y_noisy
    
def non_func(n):
    x = np.linspace(1,10,n)
    y = 1.6345 - 0.6235*np.cos(0.6067*x) - 1.3501*np.sin(0.6067*x) - 1.1622 * np.cos(2*x*0.6067) - 0.9443*np.sin(2*x*0.6067)
    y_measured = add_noise(y)
    data = np.concatenate((x,y,y_measured),axis = 0)
    data = data.reshape(3,n)
    return data.T


train_data = non_func(200)

#dijelimo podatke na x, y stvarni i y s dodanim sumom
x = train_data[:,0]
y = train_data[:,1]
y_measured = train_data[:,2]

#Ucitavamo podatke
ds = SupervisedDataSet(1,1)
ds.setField('input',x.reshape(len(x),1))
ds.setField('target',y_measured.reshape(len(y_measured),1))

#izgradnja strukture mreze
net = buildNetwork(1,
                   100, # number of hidden units
                   1,
                   bias = True,
                   hiddenclass = SigmoidLayer,
                   outclass = LinearLayer
                   )

#pocetak ucenja
trainer = BackpropTrainer(net, ds, verbose = True)
trainer.trainUntilConvergence(maxEpochs = 500) #za maxEpochs = 1000 jako dobro aproksimira funkciju


# plotamo aproksimaciju dobivenu neuronskom mrezom
pylab.plot(x,[ net.activate([i]) for i in x ], linewidth = 2, color = 'blue', label = 'NN output')

# Stvarna vrijednost y
pylab.plot(x, y, linewidth = 2, color = 'red', label = 'target')
pylab.grid()
pylab.legend()
pylab.show()


    




