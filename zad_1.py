# -*- coding: utf-8 -*-
"""
Created on Thu Jan 28 17:42:19 2016

@author: Filip
"""
import numpy as np
from pybrain.tools.shortcuts import buildNetwork
from pybrain.datasets import SupervisedDataSet
from pybrain.datasets import ClassificationDataSet
from pybrain.utilities import percentError
from pybrain.tools.shortcuts import buildNetwork
from pybrain.supervised.trainers import BackpropTrainer
from pybrain.structure.modules import SoftmaxLayer

from pylab import ion, ioff, figure, draw, contourf, clf, show, hold, plot
from scipy import diag, arange, meshgrid, where
from numpy.random import multivariate_normal



data_train = np.array(200)
data_test = np.array(100)

def generate_data(n):
    #prva klasa
    n1 = n/2
    x1_1 = np.random.normal(0.0, 2, (n1,1));
    #x1_1 = .21*(6.*np.random.standard_normal((n1,1)));
    x2_1 = np.power(x1_1,2) + np.random.standard_normal((n1,1));
    y_1 = np.zeros([n1,1])
    temp1 = np.concatenate((x1_1,x2_1,y_1),axis = 1)
    
    #druga klasa
    n2 = n - n/2
    x_2 = np.random.multivariate_normal((0,10), [[0.8,0],[0,1.2]], n2);
    y_2 = np.ones([n2,1])
    temp2 = np.concatenate((x_2,y_2),axis = 1)
    
    data = np.concatenate((temp1,temp2),axis = 0)
    
    #permutiraj podatke
    indices = np.random.permutation(n)
    data = data[indices,:]
    
    return data
    
np.random.seed(242)
data_train = generate_data(200)
np.random.seed(12)
data_test = generate_data(100)

ds_train = ClassificationDataSet(2, class_labels=['input', 'target'])
ds_test = ClassificationDataSet(2, class_labels=['input', 'target'])

#ucitavanje podataka za ucenje i testiranje
for x in data_train:
    ds_train.appendLinked(x[0:2],x[2])
    
for x in data_test:
    ds_test.appendLinked(x[0:2],x[2])


ds_train._convertToOneOfMany(bounds=[0, 1])
ds_test._convertToOneOfMany(bounds=[0, 1])

print "Number of training patterns: ", len(data_train)
print "Input and output dimensions: ", ds_train.indim, ds_train.outdim
print "First sample (input, target, class):"
print ds_train['input'][0], ds_train['target'][0], ds_train['class'][0]

#struktura mreze
fnn = buildNetwork( ds_train.indim, 5, ds_train.outdim, outclass=SoftmaxLayer)
trainer = BackpropTrainer( fnn, dataset = ds_train, momentum=0.1, verbose=True, weightdecay=0.01)

#ucenje
for i in range(5):
    trainer.trainEpochs(i)

trnresult = percentError( trainer.testOnClassData(), ds_train['class'] )
tstresult = percentError( trainer.testOnClassData(dataset=ds_test ), ds_test['class'] )

#ispis epohe i greske na testnim i trening podacima
print "epoch: %4d" % trainer.totalepochs, \
        "  train error: %5.2f%%" % trnresult, \
          "  test error: %5.2f%%" % tstresult
          
#plotanje 
ticks = arange(-3.,6.,0.2)
X, Y = meshgrid(ticks, ticks)
griddata = ClassificationDataSet(2,1, nb_classes=3)
for i in xrange(X.size):
    griddata.addSample([X.ravel()[i],Y.ravel()[i]], [0])
griddata._convertToOneOfMany()  

out = fnn.activateOnDataset(griddata)
out = out.argmax(axis=1)  
out = out.reshape(X.shape)


figure(1)
ioff()  
clf()   
hold(True) 
for c in [0,1,2]:
    here, _ = where(ds_test['class']==c)
    plot(ds_test['input'][here,0],ds_test['input'][here,1],'o')
if out.max()!=out.min():  
    contourf(X, Y, out)   
ion()  
draw()  
    
